json.extract! alumno_asignatura, :id, :nota, :ponderacion, :alumno_id, :asignatura_id, :created_at, :updated_at
json.url alumno_asignatura_url(alumno_asignatura, format: :json)
