json.extract! horario, :id, :bloque, :asignatura_id, :aula_id, :created_at, :updated_at
json.url horario_url(horario, format: :json)
