class AddFieldsToAsignaturas < ActiveRecord::Migration[5.2]
  def change
    add_column :asignaturas, :fecha_inicio, :datetime
    add_column :asignaturas, :fecha_fin, :datetime
  end
end
