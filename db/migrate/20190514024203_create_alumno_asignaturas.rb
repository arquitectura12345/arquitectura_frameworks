class CreateAlumnoAsignaturas < ActiveRecord::Migration[5.2]
  def change
    create_table :alumno_asignaturas do |t|
      t.float :nota
      t.integer :ponderacion
      t.references :alumno, foreign_key: true
      t.references :asignatura, foreign_key: true

      t.timestamps
    end
  end
end
