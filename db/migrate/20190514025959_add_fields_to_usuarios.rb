class AddFieldsToUsuarios < ActiveRecord::Migration[5.2]
  def change
    add_column :usuarios, :correo, :string
    add_column :usuarios, :celular, :integer
  end
end
