Rails.application.routes.draw do
  resources :alumno_asignaturas
  resources :horarios
  resources :asignaturas
  resources :alumnos
  resources :profesors
  resources :aulas
  resources :usuarios
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
